﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SENACAR.Views
{

    public class Servico
    {

        public string Nome { get; set; }
        public float Preco { get; set; }

        public string PrecoFormatado
        {

            get { return string.Format("R$ {0}", Preco); }


        }
    }

    public partial class ListagemView : ContentPage
    {
        public List<Servico> Servicos { get; set; }

        private void ListViewServicos_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var servico = (Servico)e.Item;

            Navigation.PushAsync(new DescricaoView(servico));

        }
       
        public ListagemView()
        {
            InitializeComponent();



            this.Servicos = new List<Servico>
            {
               new Servico {Nome = "Lamborghini Urus", Preco = 2400000 },
               new Servico {Nome = "Dodge Charger 1970", Preco = 240000 },
               new Servico {Nome = "celta", Preco = 2400000 },            
            };
        }
    }
}
