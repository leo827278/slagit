﻿using SENACAR.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SENACAR.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DescricaoView : ContentPage
	{
		public DescricaoView(Servico servico)
		{
            InitializeComponent();

            this.Title = servico.Nome;

    	}

        private void Button_Clicked(object sender, EventArgs e)
        {

            Navigation.PushAsync(new AgendamentoView(this.Servico));

        }

        public partial class DescricaoView : DescricaoView
        {
            private const int LAMBORGHINE_URUS = 2400000;
            private const int DODGE_CHARGER_1970 = 240000;
            private const int CELTA = 17000;

        }

	}
}