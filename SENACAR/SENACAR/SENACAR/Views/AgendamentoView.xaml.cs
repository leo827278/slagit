﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SENACAR.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AgendamentoView : ContentPage
	{
        public Servico Servico { get; set; }
		public AgendamentoView(Servico servico)
		{
			InitializeComponent ();

            this.Title = servico.Nome;
            this.Servico = servico;
		}

        private void Button_Clicked(object sender, EventArgs e)
        {
            DisplayAlert("Sucesso!", "Agendamento realizado com sucesso! Obrigado! " "OK");
        }
    }
}